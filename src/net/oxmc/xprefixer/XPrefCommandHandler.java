package net.oxmc.xprefixer;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class XPrefCommandHandler implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] params) {
        if (!sender.isOp())
        {
            sender.sendMessage("У вас нет прав для этого!");
            return true;
        }
        if (!(sender instanceof Player))
        {
            return false;
        }
        Player player = (Player)sender;
        if (params.length == 0)
        {
            XPrefixer.plugin.displayNameManager.resetDisplayName(player.getName());
            player.sendMessage(ChatColor.GREEN + "Ваш ник над головой" + ChatColor.RED + " сброшен" + ChatColor.GREEN + ".");
        }
        else if (params.length == 1)
        {
            Player p = Bukkit.getServer().getPlayer(params[0]);
            if (p == null)
            {
                player.sendMessage(params[0] + ChatColor.GREEN + " сейчас " + ChatColor.RED + "оффлайн" + ChatColor.GREEN + ".");
                return true;
            }
            XPrefixer.plugin.displayNameManager.resetDisplayName(p.getName());
            player.sendMessage(ChatColor.GREEN + "Вы " + ChatColor.RED + "сбросили" + ChatColor.GREEN + " ник над головой у игрока: " + ChatColor.WHITE + params[0]);
        }
        else if (params.length == 2)
        {
            Player p = Bukkit.getServer().getPlayer(params[0]);
            if (p == null)
            {
                player.sendMessage(params[0] + ChatColor.GREEN + " сейчас " + ChatColor.RED + "оффлайн" + ChatColor.GREEN + ".");
                return true;
            }
            String name = ChatColor.translateAlternateColorCodes('&', params[1]);
            XPrefixer.plugin.displayNameManager.setDisplayName(p.getName(), name);
            player.sendMessage(ChatColor.GREEN + "Вы сменили ник над головой у игрока: " + ChatColor.WHITE + params[0] +
                    ChatColor.GREEN + " на: " + ChatColor.RED + params[1] + ChatColor.GREEN + ".");
        }
        else
        {
            String name = ChatColor.translateAlternateColorCodes('&', StringUtils.join(params, ' '));
            XPrefixer.plugin.displayNameManager.setDisplayName(player.getName(), name);
            player.sendMessage(ChatColor.GREEN + "Ваш ник над головой установлен на: " + ChatColor.RED + name + ChatColor.GREEN + ".");
        }
        return true;
    }
}
